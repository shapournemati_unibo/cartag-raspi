package seiot.cartag_raspi;

import java.util.Arrays;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.pi4j.jni.Serial;

import seiot.modulo_lab_3_3.common.Observable;

public class SerialHandler extends Observable implements SerialPortDataListener {

    private static final Object TERMINATING_CHAR = ".";
    private SerialPort serialPort;
    private String message;
    
    public SerialHandler(SerialPort serialPort) {
        this.message = "";
        this.serialPort = serialPort;
        System.out.println(serialPort.toString());
        serialPort.openPort();
        serialPort.addDataListener(this);
    }

    @Override
    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_AVAILABLE; 
    }

    public void write(String s) {
        serialPort.writeBytes(s.getBytes(), s.length());
    }
    
    @Override
    public void serialEvent(SerialPortEvent event) {
        if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
            return;
        byte[] newData = new byte[serialPort.bytesAvailable()];
        serialPort.readBytes(newData, newData.length);
        message = message + new String(newData);
        if (message.contains((CharSequence) TERMINATING_CHAR)) {
            notifyEvent(new SerialMessageEvent(message,serialPort.toString()));
            message = "";
        }
    }
    
}

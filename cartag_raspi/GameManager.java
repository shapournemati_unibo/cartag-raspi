package seiot.cartag_raspi;

import com.fazecast.jSerialComm.SerialPort;

import seiot.modulo_lab_3_3.devices.ObservableMotionDetectorSensor;

public class GameManager {

    public static void main(String[] args) {
        ObservableMotionDetectorSensor pir = new seiot.modulo_lab_3_3.devices.emu.ObservablePirSensor(4);
        SerialPort comPort = getArduinoSerial();
        new GameManagerController(pir,15,comPort).start();
    }

    private static SerialPort getArduinoSerial() {
        SerialPort[] ports = SerialPort.getCommPorts();
        if (ports.length == 0) {
            return null;
        } else {
            return ports[0];
        }
    }

}

package seiot.cartag_raspi;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseHandler {

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";  
    static final String DB_URL = "jdbc:mysql://localhost/car_tag?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    //  Database credentials
    static final String USER = "raspi";
    static final String PASS = "raspipsw";

    private Connection conn = null;
    private Statement stmt = null;

    public DatabaseHandler() {
        //Register JDBC driver
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void connect() {
        //Open a connection
        try {
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void gameStarted() {
        connect();
        try {
            stmt = conn.createStatement();
            String sql = "INSERT INTO `entry`(`player`, `start`, `finish`, `cars_disabled`) VALUES (1,NOW(),NULL,0)";
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        cleanup();
    }
    
    public void gameEnded() {
        connect();
        try {
            stmt = conn.createStatement();
            String sql = "UPDATE `entry` SET `finish` = NOW() WHERE `finish` is NULL";
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        cleanup();
    }

    public void increaseScore() {
        connect();
        try {
            stmt = conn.createStatement();
            String sql = "UPDATE `entry` SET `cars_disabled` =  `cars_disabled` + 1 WHERE `finish` is NULL";
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        cleanup();        
    }

    

    public String select() {
        try {
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM entry";
            ResultSet rs = stmt.executeQuery(sql);

            //Extract data from result set
            while(rs.next()){
                //Retrieve by column name
                String player = rs.getString("player");
                Date start = rs.getDate("start");
                Date finish = rs.getDate("finish");
                int cars_disabled = rs.getInt("cars_disabled");

                //Display values
                System.out.print("player: " + player);
                System.out.print(", start time: " + start);
                System.out.print(", finish time: " + finish);
                System.out.println(", cars disabled: " + cars_disabled);
            }
            //Clean-up environment
            rs.close();
            cleanup();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        return null;
    }
    
    private void cleanup() {
        try {
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}



package seiot.cartag_raspi;

import com.fazecast.jSerialComm.SerialPort;

import seiot.modulo_lab_3_3.common.BasicEventLoopController;
import seiot.modulo_lab_3_3.common.Event;
import seiot.modulo_lab_3_3.devices.MotionDetected;
import seiot.modulo_lab_3_3.devices.ObservableMotionDetectorSensor;
import seiot.modulo_lab_3_3.devices.ObservableTimer;
import seiot.modulo_lab_3_3.devices.Tick;

public class GameManagerController extends BasicEventLoopController{

    private int gameDuration;
    
    private ObservableMotionDetectorSensor pir;
    private ObservableTimer timer;
    private SerialHandler serialHandler;
    
    private enum State {IDLE, GAME_UP};
    private State currentState;

    private ObservableTimer retrySerialtimer;
    
    
    public GameManagerController(ObservableMotionDetectorSensor pir, int gameDuration, SerialPort serial) {
        currentState = State.IDLE;
        this.gameDuration = gameDuration * 1000;

        this.pir = pir;
        this.timer = new ObservableTimer();
        if (serial != null) {
            this.serialHandler = new SerialHandler(serial);
            this.startObserving(serialHandler);
        } else {
            retrySerialtimer = new ObservableTimer();
            this.startObserving(retrySerialtimer);
            retrySerialtimer.scheduleTick(10*1000);
        }
        this.startObserving(pir);
        this.startObserving(timer);
    }
    
    @Override
    protected void processEvent(Event ev) {
        switch (currentState) {
        case IDLE:
            if (ev instanceof MotionDetected) {
                System.out.println("Car entered!");
                timer.scheduleTick(gameDuration);
                if (retrySerialtimer != null) {
                    retrySerialtimer.stop();
                } else {
                    this.serialHandler.write("H");
                }
                new DatabaseHandler().gameStarted();
                currentState = State.GAME_UP;
            } else if (ev instanceof Tick) {
                System.out.println("Looking for an available serial port communication");
                SerialPort[] ports = SerialPort.getCommPorts();
                if (ports.length == 0) {
                    System.out.println("No such luck. Trying again in 10 seconds");
                    retrySerialtimer.scheduleTick(10*1000);
                } else {
                    System.out.println("Device found!");
                    this.serialHandler = new SerialHandler(ports[0]);
                }
            }
            break;
        case GAME_UP:
            if (ev instanceof Tick) {
                System.out.println("Game is over");
                this.serialHandler.write("B");
                //change website here
                new DatabaseHandler().gameEnded();
                currentState = State.IDLE;
            } else if (ev instanceof SerialMessageEvent) {
                String message = ((SerialMessageEvent) ev).message;
                System.out.println(message);
                if (message.contains("ALIVE.")) {
                    //handle disconnection timers
                } else if (message.contains("DEAD.")) {
                    //car has been disabled, game over
                    System.out.println("Game is over!");
                    this.serialHandler.write("B");
                    //change website here
                    new DatabaseHandler().increaseScore();
                    new DatabaseHandler().gameEnded();
                    currentState = State.IDLE;
                }
            }
        }
    }

}

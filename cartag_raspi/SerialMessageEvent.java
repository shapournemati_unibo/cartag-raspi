package seiot.cartag_raspi;

import seiot.modulo_lab_3_3.common.Event;

public class SerialMessageEvent implements Event {

    public String message;
    public String deviceName;
    
    public SerialMessageEvent(String message, String deviceName) {
        this.message = message;
        this.deviceName = deviceName;
    }

}
